var subdomain = require("express-subdomain");
var express = require("express");
const { json } = require("body-parser");

const api = require("./routes/foodizone");
const shgardi = require("./routes/shgardi");
const jahez = require("./routes/jahez");

const app = express();
app.set("trust proxy", true);
app.use(json());

app.use(subdomain("api", api));
app.use(subdomain("shgardi", shgardi));
app.use(subdomain("jahez", jahez));

module.exports = app;
