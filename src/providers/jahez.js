const axios = require("axios");
const headers = {
  headers: {
    "Content-Type": "application/json",
    Authorization: "Bearer " + process.env.JAHEZ_AUTH_TOKEN,
    "X-Api-Key": process.env.JAHEZ_API_KEY,
    Accept: "application/json",
  },
};

const updateMenu = async (brandId, menu) => {
  await axios
    .post(
      "https://integration-api-sandbox.jahez.net/categories/hide_all",
      {},
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  await axios
    .post(
      "https://integration-api-sandbox.jahez.net/products/hide_all",
      {},
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  await axios
    .post(
      "https://integration-api-sandbox.jahez.net/categories/categories_upload",
      menu.categories,
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  await axios
    .post(
      "https://integration-api-sandbox.jahez.net/products/products_upload",
      menu.products,
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  return { status: true, message: "Menu updated" };
};

const updateProductAvailability = async (brandId, productId, data) => {
  await axios
    .post(
      `https://integration-api-sandbox.jahez.net/products/visibility?productId=${productId}`,
      data,
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  return { status: true, message: "Product visibility has changed" };
};

const updateBranchAvailability = async (brandId, branchId, data) => {
  await axios
    .post(
      `https://integration-api-sandbox.jahez.net/branches/visibility`,
      data,
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  return { status: true, message: "Branch visibility has changed" };
};

const onOrderCreated = async (webHookData) => {
  console.log(webHookData);
  return { status: true, message: "Order created" };
};

const onOrderUpdated = async (webHookData) => {
  console.log(webHookData);
  return { status: true, message: "Order updated" };
};

const acceptOrder = async (brandId, orderId) => {
  await axios
    .post(
      `https://integration-api-sandbox.jahez.net/webhooks/status_update`,
      { jahezOrderId: orderId, status: "A" },
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  return { status: true, message: "Order accepted" };
};

const rejectOrder = async (brandId, orderId, reason) => {
  await axios
    .post(
      `https://integration-api-sandbox.jahez.net/webhooks/status_update`,
      { jahezOrderId: orderId, status: "R", reason: reason },
      headers
    )
    .catch((err) => {
      return { status: false, message: err.message };
    });

  return { status: true, message: "Order rejected" };
};
