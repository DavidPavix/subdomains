const app = require("./app");
require("dotenv").config();

const start = async () => {
  app.listen(8000, () => {
    console.log("Listening on port 8000");
  });
};

start();
