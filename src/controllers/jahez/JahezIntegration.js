const axios = require("axios");

class JahezIntegration {
  updateMenu = async (req, res) => {
    const headers = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + process.env.JAHEZ_AUTH_TOKEN,
        "X-Api-Key": process.env.JAHEZ_API_KEY,
      },
    };
    const { categories, products } = req.body;

    await axios
      .post(
        "https://integration-api-sandbox.jahez.net/categories/hide_all",
        {},
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    await axios
      .post(
        "https://integration-api-sandbox.jahez.net/products/hide_all",
        {},
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    await axios
      .post(
        "https://integration-api-sandbox.jahez.net/categories/categories_upload",
        categories,
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    await axios
      .post(
        "https://integration-api-sandbox.jahez.net/products/products_upload",
        products,
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    res.status(200).send({ message: "Jahez menu has been updated" });
  };

  updateProductAvailability = async (req, res) => {
    const headers = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + process.env.JAHEZ_AUTH_TOKEN,
        "X-Api-Key": process.env.JAHEZ_API_KEY,
        Accept: "application/json",
      },
    };

    const productId = req.params.id;

    await axios
      .post(
        `https://integration-api-sandbox.jahez.net/products/visibility?productId=${productId}`,
        req.body,
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    res.status(200).send({ message: "Product visibility has changed" });
  };

  updateBranchAvailability = async (req, res) => {
    const headers = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + process.env.JAHEZ_AUTH_TOKEN,
        "X-Api-Key": process.env.JAHEZ_API_KEY,
        Accept: "application/json",
      },
    };

    await axios
      .post(
        `https://integration-api-sandbox.jahez.net/branches/visibility`,
        req.body,
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    res.status(200).send({ message: "Branch visibility has changed" });
  };

  onOrderCreated = async (req, res) => {
    console.log(req.body);
    res.status(200).send({ message: "Order created" });
  };

  onOrderUpdated = async (req, res) => {
    console.log(req.body);
    res.status(200).send({ message: "Order updated" });
  };

  acceptOrder = async (req, res) => {
    const headers = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + process.env.JAHEZ_AUTH_TOKEN,
        "X-Api-Key": process.env.JAHEZ_API_KEY,
        Accept: "application/json",
      },
    };

    const orderId = req.params.id;

    await axios
      .post(
        `https://integration-api-sandbox.jahez.net/webhooks/status_update`,
        { jahezOrderId: orderId, status: "A" },
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    res.status(200).send({ message: "Order accepted" });
  };

  rejectOrder = async (req, res) => {
    const headers = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + process.env.JAHEZ_AUTH_TOKEN,
        "X-Api-Key": process.env.JAHEZ_API_KEY,
        Accept: "application/json",
      },
    };

    const orderId = req.params.id;
    const reason = req.body.reason;

    await axios
      .post(
        `https://integration-api-sandbox.jahez.net/webhooks/status_update`,
        { jahezOrderId: orderId, status: "R", reason: reason },
        headers
      )
      .catch((err) => {
        res.status(400).send({ error: err.message });
      });

    res.status(200).send({ message: "Order rejected" });
  };
}

module.exports = new JahezIntegration();
