var express = require("express");
const signin = require("../controllers/foodizone/signin");

const router = express.Router();

router.post("/signin", signin);

module.exports = router;
