var express = require("express");
const jahez = require("../controllers/jahez/JahezIntegration");

const router = express.Router();

router.post("/menu/update", jahez.updateMenu);
router.post("/product/visibility/:id", jahez.updateProductAvailability);
router.post("/branch/visibility", jahez.updateBranchAvailability);
router.post("/orders/create", jahez.onOrderCreated);
router.post("/orders/update", jahez.onOrderUpdated);
router.post("/orders/accept/:id", jahez.acceptOrder);
router.post("/orders/reject/:id", jahez.rejectOrder);

module.exports = router;
