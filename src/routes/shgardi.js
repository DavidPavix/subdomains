var express = require("express");
const getMyOrders = require("../controllers/shgardi/getMyOrders");
const placeOrder = require("../controllers/shgardi/placeOrder");

const router = express.Router();

router.post("/orders", placeOrder);
router.get("/orders", getMyOrders);

module.exports = router;
